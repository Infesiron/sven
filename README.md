# Hotel Advisor "HotStar"

"HotStar" is a prototype of the application, where users can add hotels which they have visited and put a rating for them.
Main goal of this project is to create fully functional, useful web application that meets requirements of the customer.
To achive our objective we are using modern technologies, such as Ruby on rails framework, devise, haml, simple_form, carriervawe and many others.

## Requirements

Requirements:
* Ruby 2.5.1
* Rails 5.2.0
* Puma 3.1.1
* PostreSQL

## Installing

1. Clone or download repository
for example:
```
$git clone -b branch git@gitlab.com:Infesiron/sven.git
```
2. Run bundle install, make sure gems are installed properly
for example:
```
$bundle install
```
3. Add secret key to devise initiaizer

4. Migrate database
for example:
```
$rake dr:migrate
```

5. Run rails server in project directory
for example:
```
$rails server
```

## Authors

* **Roman Volovik** - *Back end developer* - [Infesiron](https://gitlab.com/Infesiron)
* **Zhadan George** - *Front end developer* - [Zhora_](https://gitlab.com/Zhora_)
* **Anastasia Voloshinskaya** - *Front end developer* - [Anavol777](https://gitlab.com/Anavol777)

## Acknowledgments

Many thanks to authors and contributors of projects which were used to create this application.
