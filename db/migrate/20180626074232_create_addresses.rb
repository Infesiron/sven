class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :country, null: false
      t.string :state
      t.string :city, null: false
      t.string :street, null: false
      t.timestamps
    end
  end
end
