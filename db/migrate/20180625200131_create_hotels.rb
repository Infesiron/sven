class CreateHotels < ActiveRecord::Migration[5.2]
  def change
    create_table :hotels do |t|
      t.string :title, null: false
      t.integer :star_rating, null: false
      t.boolean :breakfast_included, default: false
      t.text :room_description, null: true
      t.string :photo_name, null: true
      t.integer :price_for_room, null: true
      t.integer :user_id, null: false
      t.integer :address_id, null: true
      t.timestamps
    end
  end
end
