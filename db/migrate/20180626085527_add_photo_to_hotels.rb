class AddPhotoToHotels < ActiveRecord::Migration[5.2]
  def up
    add_column :hotels, :photo, :string
    remove_column :hotels, :photo_name
  end

  def down
    add_column :hotels, :photo_name, :string
    remove_column :hotels, :photo
  end
end
