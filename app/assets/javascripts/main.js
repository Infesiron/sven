$(document).ready(function($) {

	"use strict";

	var loader = function() {
		
		setTimeout(function() { 
			if($('#pb_loader').length > 0) {
				$('#pb_loader').removeClass('show');
			}
		}, 700);
	};
	loader();

    var scrollWindow = function() {
        $('#logoHotel').css("display","none");
        $('#logoHotel-img').css("display","block");
        $(window).scroll(function(){
            var $w = $(this),
                st = $w.scrollTop(),
                navbar = $('.pb_navbar'),
                sd = $('.js-scroll-wrap'),
                logoHotel = $('#logoHotel'),
                imgLogoHotel = $('#logoHotel-img');

            if (st < 150) {
                if ( navbar.hasClass('scrolled') ) {
                    navbar.removeClass('scrolled sleep');
                }
            }
            if ( st > 350 ) {
                if ( !navbar.hasClass('awake') ) {
                    navbar.addClass('scrolled');
                    navbar.addClass('awake');
                    imgLogoHotel.css("display","none");
                    logoHotel.css("display","block");
                }

                if(sd.length > 0) {
                    sd.addClass('sleep');
                }
            }
            if ( st < 350 ) {
                if ( navbar.hasClass('awake') ) {
                    navbar.removeClass('awake');
                    navbar.removeClass('scrolled sleep');
                    navbar.addClass('sleep');
                    imgLogoHotel.css("display","block");
                    logoHotel.css("display","none");
                }
                if(sd.length > 0) {
                    sd.removeClass('sleep');
                }
            }
        });
    };
    scrollWindow();
});

