class Hotel < ApplicationRecord
  belongs_to :user
  belongs_to :address, optional: true
  has_many :comments

  mount_uploader :photo, HotelPhotoUploader

  validates :title, :star_rating, presence: true
  validates :price_for_room, numericality: true, allow_nil: true
  validates_numericality_of :star_rating, only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 5,
                            message: "can only be whole number between 1 and 5."

  def self.top_five_hotels
    Hotel.order(star_rating: :desc).limit(5)
  end

  def update_rating
    rating = Comment.average_hotel_rating(id)

    return if rating.nil?

    update_attribute(:star_rating, rating.round)
  end

  def blank_stars
    5 - star_rating
  end
end
