class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :hotel

  validates :message, presence: true
  validates_numericality_of :rating, only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 5,
                            message: "can only be whole number between 1 and 5."

  def self.average_hotel_rating(hotel_id)
    Comment.where(hotel_id: hotel_id).average(:rating)
  end
end
