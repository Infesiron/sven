class Address < ApplicationRecord
  has_one :hotel

  validates :country, :state, :city, :street, presence: true

  def save_if_given(address)
    result = { given: false, saved: false }

    if address.present? && given?(address)
      self.country = address[:country]
      self.state = address[:state]
      self.city = address[:city]
      self.street = address[:street]

      result[:given] = true
      result[:saved] = save
    end

    result
  end

  private

  def given?(address)
    given = false

    if address[:country].present? || address[:state].present? ||
       address[:city].present? || address[:street].present?
      given = true
    end

    given
  end
end
