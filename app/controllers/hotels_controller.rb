class HotelsController < ApplicationController
  before_action :authenticate_user!, only: %i[new create update edit destroy]

  def landing
    @hotels = Hotel.top_five_hotels
    render action: :landing, layout: "landing"
  end

  def index
    @hotels = Hotel.order(created_at: :desc).page(params[:page]).per(10)
  end

  def new
    @hotel = Hotel.new
    @address = Address.new
  end

  def show
    @hotel = Hotel.find(params[:id])
    @comments = Comment.where(hotel_id: params[:id]).includes(:user).page(params[:page]).per(5)
    @comment = Comment.new
  end

  def update; end

  def edit; end

  def destroy; end

  def create
    @hotel = current_user.hotels.build(hotel_params)
    @address = Address.new

    address_status = @address.save_if_given(params[:hotel][:address])
    if address_status[:given]
      return render :new unless address_status[:saved]
      @hotel.address = @address
    end

    if @hotel.save
      redirect_to @hotel
    else
      render :new
    end
  end

  private

  def hotel_params
    params.require(:hotel).permit(:title, :star_rating, :breakfast_included, :room_description, :price_for_room, :photo)
  end
end
