class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @comment = current_user.comments.build(comment_params)
    hotel_id = params[:hotel_id]

    unless @comment.save
      return redirect_to hotel_path(hotel_id),
                         flash: { alert: @comment.errors.full_messages }
    end

    update_hotel_if_present(hotel_id)

    redirect_to hotel_path(hotel_id)
  end

  private

  def comment_params
    params.require(:comment).permit(:rating, :message, :hotel_id)
  end

  def update_hotel_if_present(hotel_id)
    hotel = Hotel.find(hotel_id)
    hotel.update_rating if hotel.present?
  end
end
