require 'test_helper'

class HotelTest < ActiveSupport::TestCase
  test "should update hotel rating with comments" do
    hotel = Hotel.find(1)
    assert hotel.update_rating
  end

  test "should not update hotel rating without comments" do
    hotel = Hotel.create(title: 'Test hotel', star_rating: 5, user_id: 1)
    assert_nil hotel.update_rating
  end
end
