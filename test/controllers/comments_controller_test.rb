require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
  def setup
    sign_in email: 'test@example.org', password: '123'
  end

  test "should create comment and update hotel rating" do
    hotel_id = 1

    post "/hotels/#{hotel_id}/comments",
         params: {
           comment: {
             rating: 5,
             message: "Cool hotel!",
             hotel_id: hotel_id
           }
         }
    assert_response :redirect
    follow_redirect!
    assert_response :success

    hotel = Hotel.find(hotel_id)
    assert_equal 4, hotel.star_rating
    assert_select "div.star", 4
  end

  test "should not create comment" do
    hotel_id = 1

    post "/hotels/#{hotel_id}/comments",
         params: {
           comment: {
             rating: 3,
             hotel_id: hotel_id
           }
         }
    assert_not_nil flash[:alert]
  end

  test "should display comments on hotel page" do
    get hotel_path(1)
    assert_response :success
    assert_select "div[class=comment]"
  end
end
