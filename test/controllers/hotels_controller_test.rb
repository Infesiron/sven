require 'test_helper'

class HotelControllerTest < ActionDispatch::IntegrationTest
  def setup
    sign_in email: "test@example.org", password: "123"
  end

  test "should get landing" do
    get root_url
    assert_response :success
  end

  test "should get index" do
    get hotels_url
    assert_response :success
  end

  test "should create hotel without address" do
    star_rating = 4

    get new_hotel_url
    assert_response :success

    post "/hotels",
         params: { hotel: { title: "Some hotel", star_rating: star_rating } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_select "p.card_info_text", false
  end

  test "should not create hotel with partial filled address" do
    star_rating = 4
    get "/hotels/new"
    assert_response :success

    post "/hotels",
         params: {
           hotel: {
             title: "Some hotel",
             star_rating: star_rating,
             address: { country: "US", city: "New York" }
           }
         }
    assert_response :success
  end
end
