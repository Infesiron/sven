Rails.application.routes.draw do
  root 'hotels#landing'

  devise_for :users, path: '', skip: %i[registrations], path_names: {
    sign_in: 'login',
    sign_out: 'logout'
  }
  as :user do
    get "/sign_up" => "devise/registrations#new", :as => :new_user_registration
    post "/" => "devise/registrations#create", :as => :user_registration
  end

  resources :hotels do
    resources :comments, only: %i[create]
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
